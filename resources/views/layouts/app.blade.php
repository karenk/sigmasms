<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Система отправки сообщений</title>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    <!-- Optional theme -->
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">

    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/menu.css" rel="stylesheet">

    <link href="/css/app.css" rel="stylesheet">
    <!-- Styles -->

    <!-- Scripts -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>

    @yield('head')
</head>
<body>
<div id="app">
    <div class="flex-center position-ref full-height">

        <div class="nav-side-menu">
            <div class="brand"></div>
            <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>

            <div class="menu-list">

                <ul id="menu-content" class="menu-content collapse out">
                    <li>
                        <a href="{{url('/')}}">
                            <i class="fa fa-dashboard fa-lg"></i> Главная
                        </a>
                    </li>


                    @if (Auth::guest())
                        <li data-toggle="collapse" data-target="#profile" class="collapsed">
                            <a href="#"><i class="fa fa-user fa-lg"></i> Профиль <span class="arrow"></span></a>

                            <ul class="sub-menu collapse" id="profile">
                                <li><a href="{{ url('/login') }}">Логин</a></li>
                                <li><a href="{{ url('/register') }}">Регистрация</a></li>
                            </ul>
                        </li>
                    @else
                        <li data-toggle="collapse" data-target="#profile" class="collapsed">
                            <a href="#"><i class="fa fa-user fa-lg"></i> Профиль <span class="arrow"></span></a>

                            <ul class="sub-menu collapse" id="profile">
                                <li>
                                    <a href="{{ url('/login') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Выйти
                                    </a>

                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST"
                                          style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>

                        <li>
                            <a href="{{url('/settings')}}">
                                <i class="fa fa-cog fa-lg"></i> Настройки </a>
                        </li>
                    @endif
                </ul>
            </div>
        </div>

        <div class="main-content">
            @yield('content')
        </div>

    </div>
</div>

<!-- Scripts -->
<script src="/js/app.js"></script>
</body>
</html>
