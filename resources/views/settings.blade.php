<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Система отправки сообщений</title>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    <!-- Optional theme -->
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">

    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/menu.css" rel="stylesheet">

    <link href="/css/app.css" rel="stylesheet">
    <!-- Styles -->

    <!-- Scripts -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!--custom styles-->
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">

    <!-- AngularJS -->
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular.min.js"></script>

</head>
<body>

<div id="app">
    <div class="flex-center position-ref full-height">

        <div class="nav-side-menu">
            <div class="brand"></div>
            <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>

            <div class="menu-list">

                <ul id="menu-content" class="menu-content collapse out">
                    <li>
                        <a href="{{url('/')}}">
                            <i class="fa fa-dashboard fa-lg"></i> Главная
                        </a>
                    </li>


                    @if (Auth::guest())
                        <li data-toggle="collapse" data-target="#profile" class="collapsed">
                            <a href="#"><i class="fa fa-user fa-lg"></i> Профиль <span class="arrow"></span></a>

                            <ul class="sub-menu collapse" id="profile">
                                <li><a href="{{ url('/login') }}">Логин</a></li>
                                <li><a href="{{ url('/register') }}">Регистрация</a></li>
                            </ul>
                        </li>
                    @else
                        <li data-toggle="collapse" data-target="#profile" class="collapsed">
                            <a href="#"><i class="fa fa-user fa-lg"></i> Профиль <span class="arrow"></span></a>

                            <ul class="sub-menu collapse" id="profile">
                                <li>
                                    <a href="{{ url('/login') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Выйти
                                    </a>

                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST"
                                          style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>

                        <li>
                            <a href="{{url('/settings')}}">
                                <i class="fa fa-cog fa-lg"></i> Настройки </a>
                        </li>
                    @endif
                </ul>
            </div>
        </div>

        <div class="main-content">
            @verbatim

                <div class="settings" ng-app="smsSettingsApp" ng-controller="smsSettingsController">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <h1>{{lexikon.title}}</h1>
                                <form>
                                    <div class="form-inline mb-10" ng-repeat="setting in settingsList">
                                        <div class="form-group">
                                            <label>{{settingsListLabels.fromText}}</label>
                                            <br>
                                            <input class="form-control" ng-model="setting.from" required
                                                   value="{{setting.from}}">
                                            <br>
                                            <div class="checkbox">
                                                <label><input ng-model="setting.from_is_regexp"
                                                              type="checkbox">{{settingsListLabels.regexText}}</label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>{{settingsListLabels.toText}}</label>
                                            <br>
                                            <input class="form-control" ng-model="setting.to" required
                                                   value="{{setting.to}}">
                                            <br>
                                            <div class="checkbox">
                                                <label><input ng-model="setting.to_is_regexp"
                                                              type="checkbox">{{settingsListLabels.randText}}</label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="{{setting.targetText.name}}">{{settingsListLabels.targetText}}</label>
                                            <br>
                                            <select class="form-control" ng-model="setting.target"
                                                    ng-options="y.name for (x, y) in targets">
                                            </select>
                                            <br>
                                            <br>
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-danger"
                                                    ng-click="buttons.remove.handler($index, setting.id)">Удалить</button>
                                        </div>
                                    </div>
                                    <div class="form-inline">
                                        <button type="submit" class="btn btn-primary"
                                                ng-click="buttons.add.handler()">Добавить</button>
                                        <br>
                                    </div>
                                    <div class="form-inline">
                                        <hr>
                                        <button type="submit" class="btn btn-success"
                                                ng-click="buttons.save.handler()">Сохранить</button>
                                        <!--
                                             <hr>
                                             <button type="submit" class="btn btn-default"
                                                     ng-click="buttons.test.handler()">test</button>
                                                     -->
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            @endverbatim
        </div>
    </div>
</div>
</body>
<script type="text/javascript" src="{{ asset('js/settings.js') }}"></script>

</html>
