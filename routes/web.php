<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});


Auth::routes();

Route::get('/settings', function (){
    return view('settings');
});

Route::post('/settings/save', 'SmsConverterController@settingsSave');

Route::post('/settings/load', 'SmsConverterController@settingsLoad');

Route::post('/settings/remove', 'SmsConverterController@settingsRemoveById');

Route::get('/home', function (){
    return redirect('/settings');
});

Route::get('/message/status/update', 'SmsConverterController@messageStatusesUpdateAction');