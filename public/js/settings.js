var smsSettingsApp = angular.module('smsSettingsApp', []);

smsSettingsApp.controller('smsSettingsController', function ($scope, $http, $filter) {
    $scope.lexikon = {
        title: 'Настройки',
        from: 'Ищем текст:',
        to: 'Заменяем на текст:',
        regexp: 'Использовать регулярные выражения',
        rand: 'Замена на рандомное число: int(3)',
        target: 'Где искать текст'
    };

    $scope.targets = [
        {id: 1, name: "Искать в 'сообщении'"},
        {id: 2, name: "Искать в 'имя отправителя'"}
    ];

    $scope.settingsListLabels = {
        fromText: $scope.lexikon.from,
        toText: $scope.lexikon.to,
        regexText: $scope.lexikon.regexp,
        randText: $scope.lexikon.rand,
        targetText: $scope.lexikon.target
    };

    $scope.settingsList = [];

    $scope.loadSettings = function () {
        $http({
            url: window.location.href + "/load",
            method: "POST",
            headers: {
                'Content-Type': 'application/json; charset=utf-8'
            }
        })
            .success(function (response, status, headers, config) {
                if (!response.success) {
                    alert('Ошибка! Подробно в логе.')
                    console.log(response);
                } else {
                    angular.forEach(response.data, function (item, index) {
                        $scope.settingsList.push({
                            id: item.id,
                            from: item.from,
                            from_is_regexp: (item.from_is_regexp == true),
                            to: item.to,
                            to_is_regexp: (item.to_is_regexp == true),
                            target: $scope.targets[parseInt(item.target)-1]
                        });
                    });
                }
            })
            .error(function (data, status, headers, config) {
                console.log(status);
            });

    };

    $scope.deleteSettingsItem = function (id) {
        $http({
            url: window.location.href + "/remove",
            method: "POST",
            data: {id: id},
            headers: {
                'Content-Type': 'application/json; charset=utf-8'
            }
        })
            .success(function (response, status, headers, config) {
                if (!response.success) {
                    alert('Ошибка! Подробно в логе.')
                    console.log(response);
                } else {
                    console.log(response.data);
                }
            })
            .error(function (data, status, headers, config) {
                console.log(status);
            });

    };
    $scope.loadSettings();

    $scope.buttons = {
        save: {
            text: 'Сохранить',
            handler: function () {
                $http({
                    url: window.location.href + "/save",
                    method: "POST",
                    data: $scope.settingsList,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    }
                })
                    .success(function (data, status, headers, config) {
                        if (!data.success) {
                            alert('Ошибка! Подробно в логе.')
                            console.log(data);
                        }
                    })
                    .error(function (data, status, headers, config) {
                        console.log(status);
                    });
            }
        },
        add: {
            text: 'Добавить',
            handler: function () {
                $scope.settingsList.push({
                    from: '',
                    from_is_regexp: false,
                    to: '',
                    to_is_regexp: false,
                    target: $scope.targets[0].id
                });
            }
        },
        remove: {
            text: 'Удалить',
            handler: function (index, id) {
                if (index > -1) {
                    $scope.settingsList.splice(index, 1);
                    if (parseInt(id) > 0) {
                        $scope.deleteSettingsItem(id);
                    }
                }
            }
        },
        test: {
            handler: function () {
                $http({
                    url: "http://sigmasms.loc/api/message/send",
                    method: "POST",
                    data: {
                        "user": "rss2",
                        "pass": "860a45b3c5fbb1db62d822ad5d74b369",
                        "target": "+79500237067",
                        "action": "post",
                        "type": "SMS",
                        "sender": "Имя отправителя",
                        "message": "Test message prod"
                    },
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    }
                })
                    .success(function (response, status, headers, config) {
                        console.log(response);
                    })
                    .error(function (data, status, headers, config) {
                        console.log(status);
                    });
            }
        }
    };

});