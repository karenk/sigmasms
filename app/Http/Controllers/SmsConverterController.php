<?php
/**
 *
 */

namespace App\Http\Controllers;


use App\Config;
use App\Http\Messenger\Messenger;
use App\Models\ApiSms;
use App\Models\ApiToken;
use App\Models\Message;
use App\Models\Sms;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

class SmsConverterController extends Controller
{
    const TARGET_TYPE_MESSAGE = 1;
    const TARGET_TYPE_SENDER = 2;
    const RANDOM_INT_PATTERN = '/int\(( )*\d+( )*\)/';

    public function __construct()
    {

    }

    public function sendMessageAction(Request $request)
    {
        try {
            $tokenData = $this->checkToken($request);
            $data = (object)$request->json()->all();
            $configUserId = $tokenData->user_id;

            // Modify sms.
            $sms = new ApiSms($data);
            $sms->payload->text = $this->configure($configUserId, $sms->payload->text, self::TARGET_TYPE_MESSAGE);
            $sms->payload->sender = $this->configure($configUserId, $sms->payload->sender, self::TARGET_TYPE_SENDER);

            // Send sms.
            $messenger = new Messenger($configUserId);
            $apiResponse = $messenger->sendMessage($sms, $tokenData->token);

            if ($apiResponse == 'failed') {
                throw new \Exception($apiResponse->errar);
            }
            // Save message.
            $message = new Message();
            $message->user_id = $configUserId;
            $message->type = $sms->type;
            $message->text = $sms->payload->text;
            $message->sender = $sms->payload->sender;

            $message->recipient = $apiResponse->recipient;
            $message->status = $apiResponse->status;
            $message->error = $apiResponse->error;
            $message->message_id = $apiResponse->id;

            $message->save();
            return array(
                'success' => true,
                'data' => array(
                    'message' => (array)$sms,
                    'response' => $apiResponse
                )
            );
        } catch (\Exception $e) {
            return array('success' => false, 'message' => $e->getMessage());
        }

    }

    public function settingsLoad()
    {
        try {
            $result = $this->getAllConfigsByUser();
            return array('success' => true, 'data' => $result);
        } catch (\Exception $e) {
            return array('success' => false, 'message' => $e->getMessage());
        }
    }

    public function getApiTokenAction(Request $request)
    {
        try {
            $data = (object)$request->json()->all();
            $user = User::where('username', $data->username)->first();
            if (empty($user) || $user->count() == 0) {
                throw new \Exception('Invalid username or password!');
            }
            $messenger = new Messenger($user->id);
            return array('success' => true, 'data' => $messenger->login($data));
        } catch (\Exception $e) {
            return array('success' => false, 'message' => $e->getMessage());
        }
    }

    public function settingsSave(Request $request)
    {
        try {
            $data = $request->json()->all();
            foreach ($data as $item) {
                $this->updateRow($item);
            }
            return array('success' => true);
        } catch (\Exception $e) {
            return array('success' => false, 'message' => $e->getMessage());
        }
    }

    public function settingsRemoveById(Request $request)
    {
        try {
            $data = $request->json()->all();
            $item = Config::find($data['id']);
            $item->delete();
            return array('success' => true);
        } catch (\Exception $e) {
            return array('success' => false, 'message' => $e->getMessage());
        }
    }

    public function getMessageStatusAction(Request $request, $messageId)
    {
        try {
            $tokenData = $this->checkToken($request);
            $message = Message::where(
                array('message_id' => $messageId, 'user_id' => $tokenData->user_id)
            )->first();
            if (empty($message)) {
                throw new Exception('Message not found');
            }

            return array('success' => true, 'data' => array(
                'id' => $message->message_id,
                'status' => $message->status
            ));
        } catch (\Exception $e) {
            return array('success' => false, 'message' => $e->getMessage());
        }
    }

    public function messageStatusesUpdateAction(Request $request)
    {
        try {
            $messages = DB::table('messages')
                ->where('status', '!=', 'delivered')
                ->where('created_at', '>=', Carbon::now()->subDay())
                ->get();
            foreach ($messages->toArray() as $message) {
                try {
                    $messenger = new Messenger($message->user_id);
                    $messenger->updateMessageStatus($message->message_id);
                } catch (\Exception $e) {

                    continue;
                }
            }
        } catch (\Exception $e) {
            return array('success' => false, 'message' => $e->getMessage());
        }
    }

    //******************************************************************************************************************
    private function updateRow($requestData)
    {
        $id = (!empty($requestData['id'])) ? $requestData['id'] : 0;

        if ((int)$id > 0) {
            $row = Config::find((int)$id);
        } else {
            $row = new Config();
        }
        $row->user_id = \Auth::user()->id;
        $row->from = (!empty($requestData['from'])) ? $requestData['from'] : null;
        $row->to = (!empty($requestData['to'])) ? $requestData['to'] : null;
        $row->from_is_regexp = (!empty($requestData['from_is_regexp'])) ? (int)$requestData['from_is_regexp'] : 0;
        $row->to_is_regexp = (!empty($requestData['to_is_regexp'])) ? (int)$requestData['to_is_regexp'] : 0;
        $row->target = (!empty($requestData['target']['id'])) ? (int)$requestData['target']['id'] : self::TARGET_TYPE_MESSAGE;

        if ((int)$id > 0) {
            $row->update();
        } else {
            $row->save();
        }
    }

    public function getAllConfigsByUser()
    {

        $all = Config::where(array('user_id' => \Auth::user()->id))->get();

        return $all->toArray();
    }

    public function getConfigs($where)
    {
        $configs = Config::where($where)
            ->get();
        return $configs;
    }

    public function configure($configUserId, $text, $target)
    {
        $config = $this->getConfigs(array('target' => $target, 'user_id' => $configUserId));
        $patterns = array();
        $replacements = array();
        $from = array();
        $to = array();
        foreach ($config as $item) {

            if ($item['to_is_regexp']) {
                $item['to'] = $this->getRandomInt(trim($item['to']));
            }

            if ($item['from_is_regexp']) {
                $patterns[] = '/' . $item['from'] . '/';
                $replacements[] = $item['to'];
            } else {
                $from[] = $item['from'];
                $to[] = $item['to'];
                $text = str_replace($from, $to, $text);
            }
        }
        if (!empty($patterns)) {
            $text = preg_replace($patterns, $replacements, $text);
        }
        return $text;
    }

    private function getRandomInt($strTo)
    {
        $length = preg_match_all(self::RANDOM_INT_PATTERN, $strTo, $patternInt);
        $patternInt = current(current($patternInt));
        if ($length > 0) {
            preg_match_all('!\d+!', $strTo, $matches);
            $size = (int)current(current($matches));
            $rand = rand(0, pow(10, $size) - 1);
            $rand = sprintf("%0" . $size . "s", $rand);
            $strTo = str_replace($patternInt, $rand, $strTo);
        }
        return $strTo;
    }

    private function checkToken(Request $request)
    {
        $token = $request->header('Authorization');
        if (empty($token)) {
            throw new \Exception('Authorization failed!');
        }

        $tokenData = ApiToken::where('token', $token)->first();
        if (empty($tokenData) || $tokenData->count() == 0) {
            throw new \Exception('Authorization Error!');
        }
        return $tokenData;
    }
}
