<?php
/**
 * Created by PhpStorm.
 * User: k.kochinyan
 * Date: 03.02.2018
 * Time: 17:04
 */

namespace App\Http\Messenger;

use App\Models\ApiSms;
use App\Models\ApiToken;
use Illuminate\Support\Facades\DB;


class Messenger
{
    const API_HOST = 'https://18.194.45.201';
    const API_HOST_LOGIN = self::API_HOST . '/api/login';
    const API_HOST_SEND_MESSAGE = self::API_HOST . '/api/sendings';
    // Plus message id.
    const API_HOST_MAIN_MESSAGE_STATUS = self::API_HOST . '/api/sendings/';
    const TABLE_USERS = 'users';
    const TABLE_MESSAGES = 'messages';
    const TABLE_TOKENS = 'api_tokens';

    protected $headers = array(
        'Content-Type: application/json',
        'Accept-Encoding: gzip, deflate, sdch, br',
        'Accept-Language: en-US,en;q=0.8,ru;q=0.6',
        'Connection: keep-alive'
    );

    protected $userId;
    protected $userName;

    public function __construct($userId)
    {
        $this->userId = $userId;
    }

    /**
     * Returns token from DB, if it is updated in last day, or updating from API, and returns this.
     *
     * @return \stdClass
     * @throws \Exception
     */
    public function getUserToken()
    {
        $token = DB::table(self::TABLE_TOKENS)
            ->where('user_id', $this->userId)
            ->whereDate('updated_at', date('Y-m-d'))
            ->first();
        if (empty($token)) {
            $token = $this->updateTokenByUser();
        }
        if (empty($token)) {
            throw new \Exception('Authorization failed!');
        }
        return $token;
    }

    /**
     * Send message to API.
     *
     * @param ApiSms $sms
     * @param $token
     * @return object
     */
    public function sendMessage(ApiSms $sms, $token)
    {
        if (!empty($token)) {

            $ch = curl_init(self::API_HOST_SEND_MESSAGE);
            curl_setopt_array($ch, array(
                CURLOPT_POST => true,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_HTTPHEADER => $this->getAuthorizedHeaders($token),
                CURLOPT_POSTFIELDS => json_encode($sms)
            ));

            $response = curl_exec($ch);

            if ($response === false) {
                die(curl_error($ch));
            }

            return (object)json_decode($response, true);
        }
    }

    /**
     * Get current user and logging in with login method.
     *
     * @return \stdClass
     */
    public function updateTokenByUser()
    {
        $user = DB::table(self::TABLE_USERS)
            ->where('id', $this->userId)
            ->first();
        return $this->login($user);
    }

    /**
     * Logging in API, get token data.
     *
     * @param $user
     * @return \stdClass
     */
    public function login($user)
    {
        $data = array('username' => $user->username, 'password' => $user->password);

        $ch = curl_init(self::API_HOST_LOGIN);
        curl_setopt_array($ch, array(
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_HTTPHEADER => $this->headers,
            CURLOPT_POSTFIELDS => json_encode($data)
        ));

        $response = curl_exec($ch);

        if ($response === false) {
            die(curl_error($ch));
        }

        $responseData = (object)json_decode($response, true);

        $tokenData = new \stdClass();
        $tokenData->user_id = $this->userId;
        $tokenData->token = $responseData->token;
        $tokenData->uuid = $responseData->id;
        return $this->updateTokenDB($tokenData);
    }

    /**
     * Update token oin DB.
     *
     * @param \stdClass $tokenData
     * @return \stdClass
     */
    public function updateTokenDB(\stdClass $tokenData)
    {
        $token = DB::table(self::TABLE_TOKENS)
            ->where('user_id', $tokenData->user_id);
        if (!empty($token->first())) {
            $token->update((array)$tokenData);
        } else {
            $token = new ApiToken();
            $token->user_id = $tokenData->user_id;
            $token->uuid = $tokenData->uuid;
            $token->token = $tokenData->token;
            $token->save();
        }
        return $tokenData;
    }

    /**
     * Updating message status from remote API.
     *
     * @param string $messageId
     */
    public function updateMessageStatus($messageId)
    {
        $requestData = DB::table(self::TABLE_MESSAGES)
            ->where('message_id', $messageId)
            ->join(self::TABLE_TOKENS,
                self::TABLE_TOKENS . '.user_id', '=', self::TABLE_MESSAGES . '.user_id')
            ->select(
                array(
                    self::TABLE_TOKENS . '.token'
                )
            );
        $requestData = (object)$requestData->first();
        $token = $requestData->token;
        if (!empty($token)) {

            $ch = curl_init(self::API_HOST_MAIN_MESSAGE_STATUS . $messageId);
            curl_setopt_array($ch, array(
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_HTTPHEADER => $this->getAuthorizedHeaders($token)
            ));

            $response = curl_exec($ch);

            if ($response) {
                $data = (object)json_decode($response, true);
                $status = $data->state['status'];
                DB::table(self::TABLE_MESSAGES)
                    ->where('message_id', $messageId)
                    ->update(['status' => $status]);
            }
        }
    }

    /**
     * Returns all headers with Authorisation.
     *
     * @param $token
     * @return array
     */
    public function getAuthorizedHeaders($token)
    {
        return array_merge($this->headers, array('Authorization: ' . $token));
    }
}