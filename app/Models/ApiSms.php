<?php

namespace App\Models;


class ApiSms extends \stdClass
{
    public $recipient;
    public $type;
    public $payload;
    public $schedule; // Время запуска в UTC 2017-12-11T16:08:24.992Z

    public function __construct(\stdClass $params = null)
    {
        $this->recipient = $params->recipient;
        $this->type = $params->type;
        $this->setPayload((object)$params->payload);
        $this->schedule = $params->schedule;
    }

    /**
     * @param \stdClass|null $payload
     * @throws \Exception
     */
    public function setPayload(\stdClass $payload = null)
    {
        if (empty($payload->sender) || empty($payload->text)) {
            throw new \Exception('Payload structure failed!');
        }
        $this->payload = (object)array(
            'sender' => $payload->sender,
            'text' => $payload->text
        );
    }

}
