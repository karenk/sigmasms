<?php

namespace App\Models;


class Sms extends \stdClass
{
    public $user;
    public $pass;
    public $type;
    public $action;
    public $message;
    public $target;
    public $sender;

    public function __construct(array $params = array())
    {

        $this->user = (!empty($params['user'])) ? $params['user'] : null;
        $this->pass = (!empty($params['pass'])) ? $params['pass'] : null;
        $this->type = (!empty($params['type'])) ? $params['type'] : null;
        $this->action = (!empty($params['action'])) ? $params['action'] : null;
        $this->message = (!empty($params['message'])) ? $params['message'] : null;
        $this->sender = (!empty($params['sender'])) ? $params['sender'] : null;
    }

}
