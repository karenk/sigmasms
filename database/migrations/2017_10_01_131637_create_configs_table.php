<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Config;

class CreateConfigsTable extends Migration
{
    public $id = 'id';
    public $userId = 'user_id';
    public $from = 'from';
    public $to = 'to';
    public $fromIsRegexp = 'from_is_regexp';
    public $toIsRegexp = 'to_is_regexp';
    public $targetType = 'target';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configs', function (Blueprint $table) {
            $table->increments($this->id);
            $table->integer($this->userId);
            $table->string($this->from);
            $table->string($this->to);
            $table->boolean($this->fromIsRegexp)->default(false)->nullable();
            $table->boolean($this->toIsRegexp)->default(false)->nullable();
            $table->integer($this->targetType)->default(1)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('configs');
    }

    public function update($data, $id)
    {
        Config::where($this->id, $id)
            ->update($data);
        return $id;
    }
}
